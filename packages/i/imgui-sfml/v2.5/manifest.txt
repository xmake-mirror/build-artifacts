{
    ["windows-x64-vc142-7a8a5d9992ed4dd79988a512915e5194"] = {
        sha256 = "6f4d90fc1cd8ac0710a812114217c38696f8f9a3792eba8d93a03bcebdca3b20",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc142-7a8a5d9992ed4dd79988a512915e5194.7z",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc142-86006a8e48d34836baf86299aea04620"] = {
        sha256 = "7f723b8fc27c726da9291d046a6d2ad53da418f41bc50d3372b1f962e0ad6276",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc142-86006a8e48d34836baf86299aea04620.7z",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc141-fed06c46bb844f4e89f7cb5a73cb8ecf"] = {
        sha256 = "6f7a8722d868aee9da0b3fab611ea366c3d34b1076e77cfcba15abfcf6dbb7cb",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc141-fed06c46bb844f4e89f7cb5a73cb8ecf.7z",
        toolset = "14.16.27023"
    },
    ["windows-x64-vc141-86006a8e48d34836baf86299aea04620"] = {
        sha256 = "726418d91f8978e05f06594b7ee33fc32ba4e884b5ed85fc8b722bbf5efdac50",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc141-86006a8e48d34836baf86299aea04620.7z",
        toolset = "14.16.27023"
    },
    ["windows-x64-vc141-b11105bbcf7b4e548e96550cf6edf219"] = {
        sha256 = "d34a3cdc1d32b1e03adc47e36376af7c5082f0e1095c96a5178a9cc33b3ce7a1",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc141-b11105bbcf7b4e548e96550cf6edf219.7z",
        toolset = "14.16.27023"
    },
    ["windows-x86-vc141-36443abdec5e4237b15360425266d907"] = {
        sha256 = "2ae553abde6984aa46a1c12ff32955e30305431179bb997474fd9758f8acd2cd",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc141-36443abdec5e4237b15360425266d907.7z",
        toolset = "14.16.27023"
    },
    ["windows-x64-vc143-fed06c46bb844f4e89f7cb5a73cb8ecf"] = {
        sha256 = "e3548b102538739e27b20888843a6b21b0ddd6cb23869e365847b8c1487792be",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc143-fed06c46bb844f4e89f7cb5a73cb8ecf.7z",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc142-fed06c46bb844f4e89f7cb5a73cb8ecf"] = {
        sha256 = "3b3d78a16a30be200fed51f816df8f441b2a3c0d54446befab7eefa9ee95319e",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc142-fed06c46bb844f4e89f7cb5a73cb8ecf.7z",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc141-66b56b890a48429d9b46c28020c59512"] = {
        sha256 = "17a38792ca4485c465b22d526410bc065ed4f30c797a7d201fa61002a6d0de4f",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc141-66b56b890a48429d9b46c28020c59512.7z",
        toolset = "14.16.27023"
    },
    ["windows-x86-vc142-d3b7a3f4f12c4f9d8a615d7f8af5a68c"] = {
        sha256 = "3acd37f550a29e5bb2639b71edfe8a2ad84dd35baa9b11c37ecb081dc81c82ca",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc142-d3b7a3f4f12c4f9d8a615d7f8af5a68c.7z",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc143-b11105bbcf7b4e548e96550cf6edf219"] = {
        sha256 = "97e966efd10b2d949a0fcfda03c1d3e570b2dae2dc3add67c0d4aaa195b67d79",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc143-b11105bbcf7b4e548e96550cf6edf219.7z",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc141-d3b7a3f4f12c4f9d8a615d7f8af5a68c"] = {
        sha256 = "ccbe1a349ae9f0f027d992edd84a9685e2d406fb2a5d6de9e7c4576fbd9ce114",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc141-d3b7a3f4f12c4f9d8a615d7f8af5a68c.7z",
        toolset = "14.16.27023"
    },
    ["windows-x64-vc141-7a8a5d9992ed4dd79988a512915e5194"] = {
        sha256 = "ab96c2d97a56aa1022f6053c4f5e46d22c3f79065df01ff3dd2e837730c89c5b",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc141-7a8a5d9992ed4dd79988a512915e5194.7z",
        toolset = "14.16.27023"
    },
    ["windows-x86-vc142-66b56b890a48429d9b46c28020c59512"] = {
        sha256 = "d2df13c22a227e46853b38cd083b240fd2848562b563a17300c916e4a98ce4f9",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc142-66b56b890a48429d9b46c28020c59512.7z",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc142-b11105bbcf7b4e548e96550cf6edf219"] = {
        sha256 = "e0466ffce4df632e0ed24101782a22aacafb518826d4f07a3bc24368a7f0ed12",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc142-b11105bbcf7b4e548e96550cf6edf219.7z",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc143-66b56b890a48429d9b46c28020c59512"] = {
        sha256 = "d6030260c0d2ad074dd1051ae0d47cd44174899f0fac658378e2602ada4197b4",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc143-66b56b890a48429d9b46c28020c59512.7z",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-10c08d9ecd2b4086a3fbb12f4783c918"] = {
        sha256 = "5c76518dcd60f5104b04f75d67d71ee7440599b18e89c227afc13ca580ffddd4",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc142-10c08d9ecd2b4086a3fbb12f4783c918.7z",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc143-86006a8e48d34836baf86299aea04620"] = {
        sha256 = "42733a3143f9db6275a560ef0c9a58109209a90f56f120008747b70625a13aed",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc143-86006a8e48d34836baf86299aea04620.7z",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc143-d3b7a3f4f12c4f9d8a615d7f8af5a68c"] = {
        sha256 = "665775353024517d5c6d554a2d801523f92bd2af465d04b0a6d37efbe2a036d4",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc143-d3b7a3f4f12c4f9d8a615d7f8af5a68c.7z",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc143-7a8a5d9992ed4dd79988a512915e5194"] = {
        sha256 = "8a7312464878add7a70d83876e2432b1c74b661029a71b00ade5e8ddf0d26ee7",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x64-vc143-7a8a5d9992ed4dd79988a512915e5194.7z",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc143-36443abdec5e4237b15360425266d907"] = {
        sha256 = "dff5344811d3c11987380ed545f31e4bf94649225baeb859aa721086d0bd72f3",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc143-36443abdec5e4237b15360425266d907.7z",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-36443abdec5e4237b15360425266d907"] = {
        sha256 = "f7d64530d7d0183b0358c6b1fba2100e6fef4cb2283a6dc4e3c589d01200fbc0",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc142-36443abdec5e4237b15360425266d907.7z",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc141-10c08d9ecd2b4086a3fbb12f4783c918"] = {
        sha256 = "5a1baabb5fdb865d94c7671d1fb653cf5aea2ac98cf0648b5d52a2be319189e3",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc141-10c08d9ecd2b4086a3fbb12f4783c918.7z",
        toolset = "14.16.27023"
    },
    ["windows-x86-vc143-10c08d9ecd2b4086a3fbb12f4783c918"] = {
        sha256 = "e52f9ed8f916181f14d7e9a30320ebc4f9636c9e05e7ad2960711cf9913f7645",
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/imgui-sfml-v2.5/windows-x86-vc143-10c08d9ecd2b4086a3fbb12f4783c918.7z",
        toolset = "14.32.31326"
    }
}