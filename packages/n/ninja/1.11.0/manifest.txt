{
    ["windows-x64-vc143-50da8754425740c3b120f205a48102b5"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc143-50da8754425740c3b120f205a48102b5.7z",
        sha256 = "84150bb906c54b2d1a8ef3c7d991ad95abe9abd0bef9c216c37da94d0d1718dc",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-d9761eeb64f54804aa40b50093e95da6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc142-d9761eeb64f54804aa40b50093e95da6.7z",
        sha256 = "cf787cba09091852b6e17d0183cf5f1fafff066f03d7ce367e3331d875e93e9c",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc143-1fcd100afcfd4701993f32d823452014"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc143-1fcd100afcfd4701993f32d823452014.7z",
        sha256 = "6df2983efd12ac96fd0969fba22f92b9d30f6a87666c3b0ee3b5e77352ed8c3a",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc143-d9761eeb64f54804aa40b50093e95da6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc143-d9761eeb64f54804aa40b50093e95da6.7z",
        sha256 = "884e75faf2f48ab642c24f9de28428a000fb4d74f348cab8b4a18417c5d7d21f",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc142-d9761eeb64f54804aa40b50093e95da6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc142-d9761eeb64f54804aa40b50093e95da6.7z",
        sha256 = "3c194fc5adbbeba4f55d6bbe194ab56d8d4f97500d30e4c82e6f1843213118a8",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc143-50da8754425740c3b120f205a48102b5"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc143-50da8754425740c3b120f205a48102b5.7z",
        sha256 = "26d6b604eb7c65fe9113e6e043e265bd8c9aca91060ca7b3c59baf18348e2737",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc143-d9761eeb64f54804aa40b50093e95da6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc143-d9761eeb64f54804aa40b50093e95da6.7z",
        sha256 = "27e7e75a39a530b82d27800f1be75f85892d36d5746c0960b0af49b3c7c2321c",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-50da8754425740c3b120f205a48102b5"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc142-50da8754425740c3b120f205a48102b5.7z",
        sha256 = "2fad0cc3e6165c01e92f2f6512000c7e041fe335a2b6fd9e3ea4f0f48fcdba08",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc142-50da8754425740c3b120f205a48102b5"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc142-50da8754425740c3b120f205a48102b5.7z",
        sha256 = "bece9ed52c1c7b5877d0c1ec8bf3dcf91d6f356d13189da8e2b60063da8109bf",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc142-921a392b76394a7da76e1217a4ba209a"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc142-921a392b76394a7da76e1217a4ba209a.7z",
        sha256 = "5edab4d3ec37ee5a8556fb9479458f4f2cdfca1531a059ff201c47b3d05d9303",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc143-1fcd100afcfd4701993f32d823452014"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc143-1fcd100afcfd4701993f32d823452014.7z",
        sha256 = "549bd8363e87085e9a308134fbb494539f7b92808ad3be09e2e323cdab6042e7",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc143-921a392b76394a7da76e1217a4ba209a"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc143-921a392b76394a7da76e1217a4ba209a.7z",
        sha256 = "4dbc5eb717a15807cad84355f0070b54f76e3d43de976a1594bad9d200a4ec70",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-1fcd100afcfd4701993f32d823452014"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc142-1fcd100afcfd4701993f32d823452014.7z",
        sha256 = "4a4a24afd8ddd33dd6dce82c63cfe88e39d623ba2f662757bf9cac367f515ab0",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc143-921a392b76394a7da76e1217a4ba209a"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x86-vc143-921a392b76394a7da76e1217a4ba209a.7z",
        sha256 = "a897410709a6c5192cb963bd2aec2324473157f0b76e6c8235c5071c55edea6e",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc142-1fcd100afcfd4701993f32d823452014"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc142-1fcd100afcfd4701993f32d823452014.7z",
        sha256 = "c3f04262208ab1d00259bd2ad817a23085c7a9ed934f41ad2873db5645bd07ad",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc142-921a392b76394a7da76e1217a4ba209a"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/ninja-1.11.0/windows-x64-vc142-921a392b76394a7da76e1217a4ba209a.7z",
        sha256 = "52fa76d379315ea26fdde4005ee32c9fa2f4b0b27a02d481dc5a32f20653a2b4",
        toolset = "14.29.30133"
    }
}