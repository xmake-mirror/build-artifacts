{
    ["windows-x64-vc142-6fcaba537fec4a3997b5608b16df3c02"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc142-6fcaba537fec4a3997b5608b16df3c02.7z",
        toolset = "14.29.30133",
        sha256 = "e5e15de13d79435737a4cac2290511322df2ada60e25575572aed7af7bfd5dc0"
    },
    ["windows-x64-vc143-680e8480320c4123bb3c73d2105b051e"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc143-680e8480320c4123bb3c73d2105b051e.7z",
        toolset = "14.32.31326",
        sha256 = "a1f6804e9893173022b345cac8da2d2e0c14194d6ba5e687e44ed6233edbc95e"
    },
    ["windows-x86-vc142-4a89bc2f26b24501951a460e79028df1"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc142-4a89bc2f26b24501951a460e79028df1.7z",
        toolset = "14.29.30133",
        sha256 = "761919617683491afd314e36879d2e71329300858718abea2b53e95dcd8dae59"
    },
    ["windows-x86-vc142-8b419a2317024112aca5663c1762a870"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc142-8b419a2317024112aca5663c1762a870.7z",
        toolset = "14.29.30133",
        sha256 = "f2d6f1653c6d8e83c9bcb8117ce3cb81fdf740c49d9ea0e2fd94ee2e5dac21f7"
    },
    ["windows-x64-vc142-0e5b0309c1524cfc9586edba42b96f0e"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc142-0e5b0309c1524cfc9586edba42b96f0e.7z",
        toolset = "14.29.30133",
        sha256 = "485d9ecd9462a6300e1d5d2f48a99c5b1343e7411e3c718960b7c74c1ac451c4"
    },
    ["windows-x64-vc142-572286a4040048309c8b88505b025d08"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc142-572286a4040048309c8b88505b025d08.7z",
        toolset = "14.29.30133",
        sha256 = "a900b572340c63853870e24e19e9667fa46e6e1cff25f62fcdb63b41dc97ca14"
    },
    ["windows-x64-vc143-6fcaba537fec4a3997b5608b16df3c02"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc143-6fcaba537fec4a3997b5608b16df3c02.7z",
        toolset = "14.32.31326",
        sha256 = "2acd4ed0320e7ea423b9205b3762cc7e90e16d46d8e8f0172e759188a5bc3755"
    },
    ["windows-x86-vc142-61e70ba7e15440a9ab5cfe6a98dd6367"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc142-61e70ba7e15440a9ab5cfe6a98dd6367.7z",
        toolset = "14.29.30133",
        sha256 = "32be6a8d478c800746d65a3077af58046b50c0a6d5966cfb4be13366ed88116b"
    },
    ["windows-x86-vc143-4a89bc2f26b24501951a460e79028df1"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc143-4a89bc2f26b24501951a460e79028df1.7z",
        toolset = "14.32.31326",
        sha256 = "b85b6d54abf3db0032aa52fb05051b4f5568dda1dd179b07b98925d3af195c66"
    },
    ["windows-x64-vc142-680e8480320c4123bb3c73d2105b051e"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc142-680e8480320c4123bb3c73d2105b051e.7z",
        toolset = "14.29.30133",
        sha256 = "5d13311611b7e9fff31bdc4e70dd496a745bb462a6b78c02660122cf08958e84"
    },
    ["windows-x64-vc143-572286a4040048309c8b88505b025d08"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc143-572286a4040048309c8b88505b025d08.7z",
        toolset = "14.32.31326",
        sha256 = "c4d163cedf5ef768e85bfcd2404e156a27e7a4e2930b8ce4ad7bf838b19d7811"
    },
    ["windows-x86-vc143-61e70ba7e15440a9ab5cfe6a98dd6367"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc143-61e70ba7e15440a9ab5cfe6a98dd6367.7z",
        toolset = "14.32.31326",
        sha256 = "3a94912f95a007ca3f7155fdb13076d90394071b9298d8e4b79424ef6502aaf1"
    },
    ["windows-x64-vc143-0e5b0309c1524cfc9586edba42b96f0e"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x64-vc143-0e5b0309c1524cfc9586edba42b96f0e.7z",
        toolset = "14.32.31326",
        sha256 = "013491ca1a3298d6abd0143282dbf8bb0c5517c4acc6ec96c739418c2211db9d"
    },
    ["windows-x86-vc142-614b0b9d72584a4287c665cf95b1421c"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc142-614b0b9d72584a4287c665cf95b1421c.7z",
        toolset = "14.29.30133",
        sha256 = "bf10f64eafcdac5e630c511b0c95f84cf2e199cdfc2ac77b94b02ab2e2856512"
    },
    ["windows-x86-vc143-614b0b9d72584a4287c665cf95b1421c"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc143-614b0b9d72584a4287c665cf95b1421c.7z",
        toolset = "14.32.31326",
        sha256 = "4b701c5e641ab5fb670046fa272225ed26d93d004b8976683dcadef26c2cef3c"
    },
    ["windows-x86-vc143-8b419a2317024112aca5663c1762a870"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/opencv-4.6.0/windows-x86-vc143-8b419a2317024112aca5663c1762a870.7z",
        toolset = "14.32.31326",
        sha256 = "a9f5e9eba7d20d9fa3c657797b20dba8679411e54e9bc7ed29cfa0343e7b841d"
    }
}