{
    ["windows-x64-vc143-d12d5df6ee884fc4af6ff4ad665607f4"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc143-d12d5df6ee884fc4af6ff4ad665607f4.7z",
        sha256 = "1e4e0ff44a81fbba5badb81090b3a1ba05732f9dbc90572186218f76c9df029d",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc142-4c053b9ea2ac4800b84b192121041015"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc142-4c053b9ea2ac4800b84b192121041015.7z",
        sha256 = "355bf3d0dac93395a18001481f71daddabf84f9b4984f4d6a855856ef83f2936",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc143-da57355e1bfd4ffd88cf3757e045cf0f"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc143-da57355e1bfd4ffd88cf3757e045cf0f.7z",
        sha256 = "891c14724728fcdb6f2f93227a08770a18befb7e0500961d519968a5b0e2beb5",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc143-4c053b9ea2ac4800b84b192121041015"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc143-4c053b9ea2ac4800b84b192121041015.7z",
        sha256 = "1ab104906767b900b35651a98183b9d7aabf3ecd6652debc11c05f00e83a2aae",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc143-1ecc23203be04e94a5b1577a2cc64cda"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc143-1ecc23203be04e94a5b1577a2cc64cda.7z",
        sha256 = "f217451212d4b1a97754e121b19623e40404ea5d32d2f715764fb8d2c59b4e81",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-1ecc23203be04e94a5b1577a2cc64cda"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc142-1ecc23203be04e94a5b1577a2cc64cda.7z",
        sha256 = "e7a8212c37b3c20ee361fe5cb5ffa142432e7bff4a9c23cb21f7793136603eb1",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc143-34ceae25b25b44f385fac45fbd28cb31"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc143-34ceae25b25b44f385fac45fbd28cb31.7z",
        sha256 = "04d583c5ef7aa3405d3402381d99c8a94c16582f24a20526cd938e8b2ffd4cda",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc143-a8a328509f7a42359a7787bc7af198e6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc143-a8a328509f7a42359a7787bc7af198e6.7z",
        sha256 = "982bce511edec8b2201ef9ba791aa2a66cf471d3eef74c58ffceb39c70660d65",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc142-da57355e1bfd4ffd88cf3757e045cf0f"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc142-da57355e1bfd4ffd88cf3757e045cf0f.7z",
        sha256 = "67ce05ac851c17b9434bacece27b5948fec7b9f7c0fdb9adc2f33aa5ea4e9719",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc142-1d7ca29a4a42442888d8df311362e9c6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc142-1d7ca29a4a42442888d8df311362e9c6.7z",
        sha256 = "61cd7824934c8981a53ae57268d8920585f04b3f9663a7721bd7f9d7cb3b72ba",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc143-b358d16ceda64c749553e1d4f8a35ed3"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc143-b358d16ceda64c749553e1d4f8a35ed3.7z",
        sha256 = "d1e40bdc7fb87f4163324c56e8c2a3466ffbb816be2f91f199ee6592004846fe",
        toolset = "14.32.31326"
    },
    ["windows-x86-vc142-34ceae25b25b44f385fac45fbd28cb31"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc142-34ceae25b25b44f385fac45fbd28cb31.7z",
        sha256 = "282e3f0050345d11d62507e78fd7aa013d27a074eef2f43f57423550fa809072",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc143-1d7ca29a4a42442888d8df311362e9c6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc143-1d7ca29a4a42442888d8df311362e9c6.7z",
        sha256 = "5051e60e94f191ceb024c97dbb4be29b4202dabfc96609c2ccf2d7ceafc058e9",
        toolset = "14.32.31326"
    },
    ["windows-x64-vc142-b358d16ceda64c749553e1d4f8a35ed3"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc142-b358d16ceda64c749553e1d4f8a35ed3.7z",
        sha256 = "ba290feb97469686d219d31bdaf5484af820cfdacab4f9e19df9277d6e7f05a1",
        toolset = "14.29.30133"
    },
    ["windows-x64-vc142-d12d5df6ee884fc4af6ff4ad665607f4"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x64-vc142-d12d5df6ee884fc4af6ff4ad665607f4.7z",
        sha256 = "80cadd5cfb60dcbeebf6af239d2df0b3d7cbaad364d05ba8259f28e40d110330",
        toolset = "14.29.30133"
    },
    ["windows-x86-vc142-a8a328509f7a42359a7787bc7af198e6"] = {
        urls = "https://github.com/xmake-mirror/build-artifacts/releases/download/rapidjson-2022.7.20/windows-x86-vc142-a8a328509f7a42359a7787bc7af198e6.7z",
        sha256 = "2e30fbaa6fc08bc1e32f9ad2139a2515031983202b0829a20005bee0d7992603",
        toolset = "14.29.30133"
    }
}